const { token, shardcount, clustercount, guildspershard } = require("./utils/variables");
const { ShardingManager } = require('kurasuta');
const { join } = require('path');
const sharder = new ShardingManager(join(__dirname, 'bot'), {
    token: token,
    shardCount: shardcount,
    clusterCount: clustercount,
    respawn: true,
    retry: true,
    guildsPerShard: guildspershard,
    clientOptions: {
        disabledEvents: ["TYPING_START", "CHANNEL_PINS_UPDATE", "VOICE_SERVER_UPDATE", "VOICE_STATE_UPDATE", "PRESENCE_UPDATE", "MESSAGE_REACTION_ADD", "MESSAGE_REACTION_REMOVE", "MESSAGE_REACTION_REMOVE_ALL", "CHANNEL_CREATE", "CHANNEL_DELETE", "CHANNEL_UPDATE", "GUILD_BAN_ADD", "GUILD_BAN_REMOVE", "GUILD_EMOJIS_UPDATE", "GUILD_INTEGRATIONS_UPDATE", "GUILD_ROLE_CREATE", "GUILD_ROLE_DELETE", "GUILD_ROLE_UPDATE"]
    },
    ipcSocket: 9989
});

sharder.spawn();