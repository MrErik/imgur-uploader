const { MessageEmbed } = require("discord.js");
const imgur = require('imgur');
exports.use = async (client, message, args, command) => {
    let image;
    if (message.attachments.size > 0) {
        let attachement_image = message.attachments.first()
        // console.log(attachement_image)
        let attachmentname = attachement_image.name.split(".");
        //  if (attachmentname[1] == "gif") return message.reply("Na only premium peeps gets to use this awesome feature!")
        image = attachement_image.url
    } else {
        image = args[0]
    }
    if (!image) return message.channel.send(`Please provide me with a link to an image or upload it as an attachment.`)

    imgur.uploadUrl(image).then(function (json) {
        const iEmbed = new MessageEmbed()
            .setDescription(`Image link: **<${json.data.link}>**`)
            .setColor("#fd01c3")
            .setImage(json.data.link)
        message.channel.send(iEmbed)
        message.delete()
    }).catch(err => {
        if (err) return message.reply("Some error occured when I tried to process your request. Please retry.")
    })
};

exports.command = {
    aliases: [""]
};