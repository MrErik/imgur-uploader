const { MessageEmbed } = require("discord.js");

exports.use = async (client, message, args, command) => {

    let guild = await client.shard.fetchClientValues('guilds.size')
    let members = await client.shard.broadcastEval('this.guilds.reduce((prev, guild) => prev + guild.memberCount, 0)')
    let channels = await client.shard.fetchClientValues('channels.size')
    let ram = await client.shard.broadcastEval('process.memoryUsage().heapUsed')
    const totalGuilds = guild.reduce((prev, guildCount) => prev + guildCount, 0);
    const totalMembers = members.reduce((prev, memberCount) => prev + memberCount, 0);
    const totalChannels = channels.reduce((prev, channelCount) => prev + channelCount, 0);
    let totalHeap = ram.reduce((prev, heap) => prev + heap, 0);
    totalHeap = totalHeap / 1024 / 1024
    const embedStats = new MessageEmbed()
        .setTitle("Imgur Uploader - Statistics")
        .setColor("#fd01c3")
        .addField("• Ram Usage", `${Math.round(totalHeap / 1024 * 100) / 100}GB`)
        .addField("• Users", totalMembers)
        .addField("• Servers", totalGuilds)
        .addField("• Channels", totalChannels)
        .addField("• API Latency", `${Math.round(client.ws.ping)} ms`)
        .addField("• Shard ID", message.guild.shardID)

    message.channel.send(embedStats).catch(err => { });
};

exports.command = {
    aliases: [""]
}