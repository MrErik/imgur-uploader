const { MessageEmbed } = require("discord.js");

exports.use = async (client, message, args, command) => {

    const helpEmbed = new MessageEmbed()
        .setColor("#fd01c3")
        .setTitle("Imgur Uploader - Help")
        .setDescription("Need more help than I can provide? Join our discord https://dropletdev.com/discord\n\n**i!upload** - Upload with a image or with a link\n**i!invite** - Invite the bot\n**i!stats** - View some stats\n**i!support** - Join our support server\n**i!vote** - Vote for the bot")

    return message.channel.send(helpEmbed).catch(err => { });

};

exports.command = {
    aliases: [""]
};