const { MessageEmbed } = require("discord.js");

exports.use = async (client, message, args, command) => {

    let supportEmbed = new MessageEmbed()
        .setColor("#fd01c3")
        .setTitle("Imgur Uploader - Support Server")
        .setDescription("Need help using the Imgur Uploader bot? Join our support server [here](https://dropletdev.com/discord)")

    return message.channel.send(supportEmbed).catch(err => { });


};

exports.command = {
    aliases: [""]
}