const { MessageEmbed } = require("discord.js");

exports.use = async (client, message, args, command) => {

    let inviteEmbed = new MessageEmbed()
        .setColor("#fd01c3")
        .setTitle("Imgur Uploader - Invite")
        .addField("Normal Invite", "You can invite Imgur Uploader by clicking [here](https://discordapp.com/api/oauth2/authorize?client_id=620673396644118559&permissions=117760&scope=bot) in order to only give it needed permissions.")
        .setURL("https://discordapp.com/api/oauth2/authorize?client_id=620673396644118559&permissions=117760&scope=bot")

    return message.channel.send(inviteEmbed).catch(err => { });
};

exports.command = {
    aliases: [""]
}