const { MessageEmbed } = require("discord.js");

exports.use = async (client, message, args, command) => {
    let voteEmbed = new MessageEmbed()
        .setColor("#fd01c3")
        .setTitle("Imgur Uploader - Vote")
        .setDescription("Voting helps us out alot! Want to vote? Click [here](https://discordbots.org/bot/550613223733329920/vote)")
        .setURL("https://discordbots.org/bot/550613223733329920/vote")

    message.channel.send(voteEmbed).catch(err => { });
};

exports.command = {
    aliases: [""]
}