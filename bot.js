const fs = require("fs");
const { token } = require("./utils/variables.js");
process.setMaxListeners(0);
const EventMitter = require("events")
const event = new EventMitter();
event.setMaxListeners(0);
const { BaseCluster } = require('kurasuta');
const DBL = require("dblapi.js");
const dbl = new DBL("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyMDY3MzM5NjY0NDExODU1OSIsImJvdCI6dHJ1ZSwiaWF0IjoxNTcwMDE2MDcwfQ.EzNJS3MDWLf5rj3Ako9kyLTkXkotsF7Giw4TtINdOYM", { webhookPort: 5014, webhookAuth: 'sljenghlsefe90fsef' });

module.exports = class extends BaseCluster {
    launch() {
        this.client.login(token)
        this.client.on("shardReady", (id) => console.log(`Shard ${id} is online.`))
        this.client.once("ready", async () => {
            this.client.user.setActivity(`i!help || Droplet`);
            setInterval(() => {
                this.client.user.setActivity(`i!help || Droplet`);
            }, 60000 * 10);
            if (this.id == 0) {
                setInterval(() => {
                    const promises = [
                        this.client.shard.fetchClientValues('guilds.size'),
                    ];
                    Promise.all(promises).then(results => {
                        if (!results) return;
                        const totalGuilds = results[0].reduce((prev, guildCount) => prev + guildCount, 0);
                        dbl.postStats(totalGuilds);
                    });
                }, 180000);
            }
        });

        this.client.on("shardDisconnected", (shard, id) => {
            console.log("Shard went down, id: " + id);
        });
        this.client.on("shardError", (err, id) => {
            console.log("Shard " + id + " had an error: " + err)
        })

        this.client.commands = new Map();
        this.client.aliases = new Map();

        fs.readdir('./commands/', (err, files) => {
            let jsfiles = files.filter(f => f.split('.')
                .pop() === 'js');
            if (jsfiles.length <= 0) {
                return;
            }
            jsfiles.forEach(f => {
                let props = require(`./commands/${f}`);
                props.fileName = f;
                this.client.commands.set(f.slice(0, -3), props);
                props.command.aliases.forEach(alias => {
                    this.client.aliases.set(alias, f.slice(0, -3));
                });
            });
        });

        this.client.on("message", async message => {
            if (message.author.bot) return;
            if (!message.guild) return;
            let prefix = "i!";
            let prefixArray = ["<@620673396644118559>", "<@!620673396644118559>", "i!", "I!"];
            for (const thisPrefix of prefixArray) {
                if (message.content.startsWith(thisPrefix)) prefix = thisPrefix;
            };
            if (!message.content.startsWith(prefix)) return;
            const args = message.content.slice(prefix.length).trim().split(/ +/g);
            let command = args.shift().toLowerCase();
            let cmd;
            if (this.client.commands.has(command)) {
                cmd = this.client.commands.get(command);
            } else if (this.client.aliases.has(command)) {
                cmd = this.client.commands.get(this.client.aliases.get(command));
            }
            if (!cmd) return;
            message.prefix = prefix;
            message.guild.members.fetch(message.author.id).catch(err => { })
            cmd.use(this.client, message, args, command);
        });
        this.client.on("error", console.error)
        process.on("uncaughtException", console.error)
        process.on("unhandledRejection", console.error)

    }
};